package deviceManager;

public class MobileDevice extends Device {
	
	String owner;
	
	public MobileDevice(String type, String make, String model, String yearManufactered, String MACNumber,  String color, String owner){
		
		super( type, make,  model, yearManufactered,  MACNumber, color);		
		this.owner = owner;

	}
	
	
}
