package deviceManager;

public class StationaryDevice extends Device {

	String room;
	
	public StationaryDevice(String type, String make, String model, String yearManufactered,  String MACNumber, String color, String room){
		
		super( type, make,  model, yearManufactered,  MACNumber, color);
		this.room = room;
	}
}
