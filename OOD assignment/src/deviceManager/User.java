package deviceManager;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

//import Lab9.Customer;

/**
 * 
 * @author Marcin Ziejewski sn.2865484
 * 
 * Programme:  HDCS
 * Module: Object-Oriented Development
 *
 *	This class is one part of Device Manager application
 *  
 *  Main purpose of this class is to use any other class or interface if 
 *  this package for creating objects and operating on it.
 *  
 *  Class will directly interact with user
 *  
 *  All methods are implemented from interface "UserInterface"
 */
public class User implements UserInterface3{
	
	/* Here are variables of type ArrayList that will be used for storing objects */
	static ArrayList<MobileDevice>mobile = new ArrayList<>();	
	static ArrayList<StationaryDevice>stationary = new ArrayList<>();
	
	String path = new File("").getAbsolutePath();
	
	String stationaryPath = path + "\\src\\textFiles\\StationaryDevice.txt";
	String mobilePath     = path + "\\src\\textFiles\\MobileDevices.txt";
	
	/* Main tread for application start */	
	public static void main(String[] args){
		
		/* This is object user of User class that will be used to call
		 * internal method in this class */
		User user = new User();
		
		/* This is call for method that will start interactive 
		 * dialog with user in order to crate some action
		 */
		    user.userInterfacePanel();
		
	}
	
	/* --------------------------------
	 * Method no. 1 UserInterfacePanel
	 * -------------------------------- */	
	/*
	 * This method is first to start when application is launched
	 * 
	 * Main purpose of this method is to:
	 * 
	 *  - display short message regarding to use of whole software
	 * 
	 *  - present all available option to user 
	 * 
	 *  - take user input (decision) and create appropriate action
	 */
	
	@Override
	public void userInterfacePanel(){
		
		/* in this block info about software is displayed */
		System.out.println("\n        -------------    Welcome user    -------------     \n");
		System.out.println("This application is for storing and managing details of all\n"
						 + "devices from our office\n\n"
						 + "Devices are group into two categories:\n\n"
						 + "1. Mobile devices\n"
						 + "2. Stationary devices\n"
						 + "\n"
						 + "Below you have list of all availiable option\n\n"
						 + "To use any of them press corresponding number on the keyboard\n");
								
		System.out.println("\n    -------------    Option availiable    -------------    \n\n");
		/* next block will use Scanner object and switch to take user 
		 * input and turn into appropriate action                   */
	
		Scanner key1 = new Scanner(System.in);
		
		System.out.println("1. If you want to see all devices press ------------------------------ 1\n");
		System.out.println("2. If you want to see all mobile devices press ----------------------- 2\n");
		System.out.println("3. If you want to see all stationary devices press ------------------- 3\n");
		System.out.println("4. If you want to search for one particular mobile device press ------ 4\n");
		System.out.println("5. If you want to search for one particular stationary device press--- 5\n");
		System.out.println("6. If you want to delete all mobile devices press -------------------- 6\n");
		System.out.println("7. If you want to delete all stationary devices press ---------------- 7\n");
		System.out.println("8. If you want to add new mobile device press ------------------------ 8\n");
		System.out.println("9. If you want to add new stationary device press -------------------- 9\n");
		
//		System.out.println("\nIf you do not want to use any of above and leave press ---- 0\n");
		
		/* this int variable will hold user's answer*/
//		int responce = key1.nextInt();
		
		String yesOrNo = null;
			

		do{	
			System.out.println("\nIf you do not want to use any of above and leave press ---- 0\n");
			/* this int variable will hold user's answer*/
			int responce = key1.nextInt();
			/* This loop will make sure that proper entry is provided 
			 * otherwise application will switch off
			 * user has three chance for providing proper entry */
			
			int counter = 0;
			/* condition in while loop sees that only integer 0 to 8 will be considered as qualifying entry */
			while((responce < 0 || responce > 9) && counter < 2 ){
				System.out.println("This is not a valid entry\nPleae check the list and try again");
				responce = key1.nextInt();
				// this counter will increment after each wrong entry
//				counter++;
			};
		
		
		/* I choose to use switch for this selection because I believe it is most 
		 * suitable for this type selection instead of either for or while*/
		switch(responce){
		
		/* each case holds code that calls appropriate methods required for one particular action, 
		 * some of them may interact with other to bring to final result */		
		case 1:
			showAllDevices();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
			
		case 2:
			showMobileDevices();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
			
		case 3:
			showStationaryDevices();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
			
		case 4:
			searchMobileDevice();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
		
			
		case 5:
			searchStationaryDevice();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
			
			
		case 6:
			deleteAllMobileDevices();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
			
		case 7:
			deleteAllStationaryDevices();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
		
		case 8:
			addMobileDevice();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;
		
		case 9:
			addStationaryDevice();
			System.out.println("\nTo make another selection press Y for yes or N for no.");
			yesOrNo = key1.next();
			break;	
			
		/* This case is initialise when user decide to leave application */	
		case 0:
			System.out.println("\nThank you for using our Manager");
			break;
		
		/* This is default case which means that it is initialise when user fail to provide
		 * qualifying entry */	
		default: 
			System.out.println("\nThank you for using our Manager");
			break;	
		}
	}while(yesOrNo.equalsIgnoreCase("Y"));
	
		System.out.println("\nThank you for using our Manager");	
}
	/* --------------------------------
	 * Method no. 1 showAllDevices
	 * -------------------------------- */	
	/*
	 * This method will call two others method 
	 * 
	 * - showMobileDevices()
	 * 
	 * - showStationaryDevices()
	 * 
	 * that both read text files where details of devices are stored
	 * and will gather results and display them together.
	 * 
	 */
		@Override
		public void showAllDevices(){
			
			
			showMobileDevices();
			System.out.println();
			showStationaryDevices();
		}	
		
	/* --------------------------------
	 * Method no. 2 showMobileDevices
	 * -------------------------------- */	
	/*
	 * This method will read text file where details of all Mobile devices are stored
	 * and display them on the console
	 * 
	 */
	
	@Override
	public void showMobileDevices() {
		
		/* In this block we have all necessary objects variables that will read text file*/
		
		// File object is created
		File mobileFile = new File(mobilePath);
		// this condition checks if file is created
		if(mobileFile.exists()){
		
			// Reader is created
			FileReader mobileReader = null;
			
			try {
				mobileReader = new FileReader(mobileFile);
			} catch (FileNotFoundException e1) {}	
			
			// buffered reader is created
			BufferedReader mobileBuffer = new BufferedReader(mobileReader);
			
			// this String array will hold detail of single device represented as array of string
			String[] singleMobileDevice = {};
			
			/* string line will hold single line of text file 
			 * 
			 * text in file is formated that details of all devices are kept in separates lines
			 * on the top of each others
			 * */
			String line = "";
			
			try {				
				// first line is readed and assign to String line 
				line = mobileBuffer.readLine();
				
				/* Buffer reads text file all the time until one of them is empty*/
				while(line != null){
					
					/* now string line is stripped from commas that separate each detail of device in the file 
					 * and assign to array of string for further processing */
					singleMobileDevice  = line.split(",");
						
						/* her we have String representation of each device detail 
						 * assigned to corresponding values in the array*/
						String type             = singleMobileDevice[0];
						String make             = singleMobileDevice[1];
						String model            = singleMobileDevice[2];
						String yearManufactered = singleMobileDevice[3];
						String MACNumber        = singleMobileDevice[4];
						String color            = singleMobileDevice[5];
						String owner            = singleMobileDevice[6];
											
						// Using above values object of mobile class is created ...
						MobileDevice m1 = new MobileDevice(type, make,  model, yearManufactered,  
														   MACNumber, color, owner);
						// ... and stored in List of devices
						mobile.add(m1);
					
					/* array is set to null*/	
					singleMobileDevice = null;
					
					/* another line is read */
					line = mobileBuffer.readLine();
					
					/* and if it is not null whole process stars again*/
				}				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			
			}			
			
			System.out.println("This the list of all mobile devices:\n"
				           	 + "------------------------------------");
			
			/* This loop will print Statement with representation of details of single device */
			for(MobileDevice n: mobile){
				System.out.println("This is mobile device: " + n.type + " made by " + n.make + " model " + n.model 
								   + " form " + n.yearManufactered + " with MAC no. - " + n.MACNumber + " color " 
						           + n.color + " and belongs to " + n.owner + "\n");			
			}			
			mobile.clear();
		}else{			
			// This message is printed in case of absence an appropriate file
			System.out.println("Sorry such file doesn't exists in our records");
		}		
	}
	
	
	/* --------------------------------
	 * Method no. 3 showStationaryDevices
	 * -------------------------------- */	
	/*
	 * This method will read text file where details of all Stationary devices are stored
	 * and display them on the console
	 * 
	 */
	
	@Override
	public void showStationaryDevices() {
		
		/* In this block we have all necessary objects variables that will read text file*/
		
		// File object is created
		File statFile = new File(stationaryPath);
		// this condition checks if file is created
		if(statFile.exists()){
		
			// Reader is created
			FileReader statReader = null;
			
			try {
				statReader = new FileReader(statFile);
			} catch (FileNotFoundException e1) {}	
			
			// buffered reader is created
			BufferedReader staticBuffer = new BufferedReader(statReader);
			
			// this String array will hold detail of single device represented as array of string
			String[] singleStaticDevice = {};
			
			/* string line will hold single line of text file 
			 * 
			 * text in file is formated that details of all devices are kept in separates lines
			 * on the top of each others
			 * */
			String line = "";
			
			try {				
				// first line is readed and assign to String line 
				line = staticBuffer.readLine();
				
				/* Buffer reads text file all the time until one of them is empty*/
				while(line != null){
					
					/* now string line is stripped from commas that separate each detail of device in the file 
					 * and assign to array of string for further processing */
					singleStaticDevice  = line.split(",");
						
						/* her we have String representation of each device detail 
						 * assigned to corresponding values in the array*/
						String type             = singleStaticDevice[0];
						String make             = singleStaticDevice[1];
						String model            = singleStaticDevice[2];
						String yearManufactered = singleStaticDevice[3];
						String MACNumber        = singleStaticDevice[4];
						String color            = singleStaticDevice[5];
						String room             = singleStaticDevice[6];
											
						// Using above values object of stationary class is created ...
						 StationaryDevice s1 = new StationaryDevice(type, make,  model, yearManufactered,  
								 									MACNumber, color, room);
						// ... and stored in List of devices
						 stationary.add(s1);
					
					/* array is set to null*/	
					singleStaticDevice = null;
					
					/* another line is read */
					line = staticBuffer.readLine();
					
					/* and if it is not null whole process stars again*/
				}				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			}	
			
			System.out.println("This the list of all stationary devices:\n"
					         + "----------------------------------------");
			
			/* This loop will print Statement with representation of details of single device */
			for(StationaryDevice n: stationary){
				System.out.println("This is stationary device: " + n.type + " made by " + n.make + " model " + n.model 
								   + " form " + n.yearManufactered + " with MAC no. - " + n.MACNumber + " color " 
						           + n.color + " and is located in room nr" + n.room + "\n");
			}
			stationary.clear();
		}else{			
			// This message is printed in case of absence an appropriate file
			System.out.println("Sorry such file doesn't exists in our records");
		}
			
	}
	
	/* --------------------------------
	 * Method no. 4 searchForMobileDevice
	 * -------------------------------- */	
	/*
	 * This method will read text file where details of all mobile devices are stored
	 * in order to find one particular device according to user needs
	 * After device is found details are displayed on console and user has a choice what to do 
	 * next with device delete or leave it as it is
	 * User will do the search based on three criteria
	 * type, make and model
	 * 
	 */
	
	public void searchMobileDevice(){		
		
		/* This list will hold result of search */		
		ArrayList<MobileDevice>mobileSearch = new ArrayList<>();	
			
		MobileDevice m1 = null;		
		/* First user entry must be taken 
		 * Scanner for user entry */ 
		
		Scanner key2 = new Scanner(System.in);
		
		/* This is variables that will hold criteria for searching */
		String typeS             = "";
		String makeS             = "";
		String modelS            = "";
		
		/* This block will present line interface for user to provide details of searched device*/
		System.out.print("\nPlease provide details of device you want to find:\n");
		System.out.print("-------------------------------------------------\n");
		System.out.print("Type: ");                             typeS = key2.nextLine();
		System.out.print("Producent: ");					    makeS = key2.nextLine();
		System.out.print("Model: ");	            		   modelS = key2.nextLine();
		
		/* Now when We have all criteria from user all what's left is to compare all devices we have in the file against it */		
		
		/* In this block we have all necessary objects variables that will read text file*/
		
		// File object is created
		File mobileFile = new File(mobilePath);
		// this condition checks if file is created
		if(mobileFile.exists()){		
		
			// Reader is created
			FileReader mobileReader = null;
			
			try {
				mobileReader = new FileReader(mobileFile);			
			
			} catch (FileNotFoundException e1) {} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
				// buffered reader is created
				BufferedReader mobileBuffer = new BufferedReader(mobileReader);
				
			
			// this String array will hold detail of single device represented as array of string
			String[] singleMobileDevice = {};
			
			/* string line will hold single line of text file 
			 * 
			 * text in file is formated that details of all devices are kept in separates lines
			 * on the top of each others
			 * */
			String line = "";
			int counter = 0;
			
			try {				
				// first line is readed and assign to String line 
				line = mobileBuffer.readLine();
				
				/* Buffer reads text file all the time until one of them is empty*/
				while(line != null){
					
					/* now string line is stripped from commas that separate each detail of device in the file 
					 * and assign to array of string for further processing */
					singleMobileDevice  = line.split(",");						
					
						/* her we have String representation of each device detail 
						 * assigned to corresponding values in the array*/
						String type             = singleMobileDevice[0];
						String make             = singleMobileDevice[1];
						String model            = singleMobileDevice[2];
						
						
						String yearManufactered = singleMobileDevice[3];
						String MACNumber        = singleMobileDevice[4];
						String color            = singleMobileDevice[5];
						String owner            = singleMobileDevice[6];
						
						
						
						// Using above values object of mobile class is created ...
						m1 = new MobileDevice(type, make,  model, yearManufactered,  
														   MACNumber, color, owner);
						// ... and stored in List of devices
						mobile.add(m1);						
						
												
						
						if(typeS.equalsIgnoreCase(type) && makeS.equalsIgnoreCase(make) && modelS.equalsIgnoreCase(model)){
							
							System.out.println("This is mobile device: " + m1.type + " made by " + m1.make + " model " + m1.model 
									   + " form " + m1.yearManufactered + " with MAC no. - " + m1.MACNumber + " color " 
							           + m1.color + " and belongs to " + m1.owner + "\n");
							
							
									   mobileSearch.add(m1);													
							
							System.out.println("Do you want to delete it? y for yes or n for no");
							String delete = key2.nextLine();
							
							if(delete.equalsIgnoreCase("y") )
								mobile.remove(m1);	
							
						}
												
					/* another line is read */
					line = mobileBuffer.readLine();
				}
			String outputLine = "";
			
			for(MobileDevice n: mobile){
				outputLine += n.type + ","+ n.make + ","+ n.model + "," + n.yearManufactered + ","
							+ n.MACNumber + ","+ n.color + ","+ n.owner + "\n";
				
			}
							
				FileWriter output = new FileWriter(mobilePath); 
				
				    output.write(outputLine);
				    output.close();				
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			
			}
			
		}else{			
			// This message is printed in case of absence an appropriate file
			System.out.println("Sorry such file doesn't exists in our records");		
		}		
	}	
	
	
	/* --------------------------------
	 * Method no. 5 searchStationaryDevice
	 * -------------------------------- */	
	/*
	 * This method will read text file where details of all stationary devices are stored
	 * in order to find one particular device according to user needs
	 * After device is found details are displayed on console and user has a choice what to do 
	 * next with device delete or leave it as it is
	 * User will do the search based on three criteria
	 * type, make and model
	 * 
	 */
	
	public void searchStationaryDevice(){		
		
		/* This list will hold result of search */
		ArrayList<StationaryDevice>stationarySearch = new ArrayList<>();
		
		StationaryDevice s1 = null;
		
		/* First user entry must be taken 
		 * Scanner for user entry */ 
		
		Scanner key2 = new Scanner(System.in);
		
		/* This is variables that will hold criteria for searching */
		String typeS             = "";
		String makeS             = "";
		String modelS            = "";
		
		/* This block will present line interface for user to provide details of searched device*/
		System.out.print("\nPlease provide details of device you want to find:\n");
		System.out.print("-------------------------------------------------\n");
		System.out.print("Type: ");                             typeS = key2.nextLine();
		System.out.print("Producent: ");					    makeS = key2.nextLine();
		System.out.print("Model: ");	            		   modelS = key2.nextLine();
		
		/* Now when We have all criteria from user all what's left is to compare all devices we have in the file against it */		
		
		/* In this block we have all necessary objects variables that will read text file*/
		
		// File object is created
		File stationaryFile = new File(stationaryPath);
		// this condition checks if file is created
		if(stationaryFile.exists()){		
		
			// Reader is created
			FileReader stationaryReader = null;
			
			try {
				stationaryReader = new FileReader(stationaryFile);			
			
			} catch (FileNotFoundException e1) {} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
				// buffered reader is created
				BufferedReader stationaryBuffer = new BufferedReader(stationaryReader);
				
			
			// this String array will hold detail of single device represented as array of string
			String[] singleStationaryDevice = {};
			
			/* string line will hold single line of text file 
			 * 
			 * text in file is formated that details of all devices are kept in separates lines
			 * on the top of each others
			 * */
			String line = "";			
			
			try {				
				// first line is readed and assign to String line 
				line = stationaryBuffer.readLine();
				
				/* Buffer reads text file all the time until one of them is empty*/
				while(line != null){
					
					/* now string line is stripped from commas that separate each detail of device in the file 
					 * and assign to array of string for further processing */
					singleStationaryDevice  = line.split(",");						
					
						/* her we have String representation of each device detail 
						 * assigned to corresponding values in the array*/
						String type             = singleStationaryDevice[0];
						String make             = singleStationaryDevice[1];
						String model            = singleStationaryDevice[2];
						
						
						String yearManufactered = singleStationaryDevice[3];
						String MACNumber        = singleStationaryDevice[4];
						String color            = singleStationaryDevice[5];
						String room            = singleStationaryDevice[6];
						
						
						
						// Using above values object of mobile class is created ...
						s1 = new StationaryDevice(type, make,  model, yearManufactered,  
														   MACNumber, color, room);
						// ... and stored in List of devices
						stationary.add(s1);						
						
												
						
						if(typeS.equalsIgnoreCase(type) && makeS.equalsIgnoreCase(make) && modelS.equalsIgnoreCase(model)){
							
							System.out.println("This is stationary device: " + s1.type + " made by " + s1.make + " model " + s1.model 
									   + " form " + s1.yearManufactered + " with MAC no. - " + s1.MACNumber + " color " 
							           + s1.color + " and belongs to " + s1.room + "\n");
							
										stationarySearch.add(s1);													
							
							System.out.println("Do you want to delete it? y for yes or n for no");
							String delete = key2.nextLine();
							
							if(delete.equalsIgnoreCase("y") )
								stationary.remove(s1);	
							
						}												
					/* another line is read */
					line = stationaryBuffer.readLine();
				}
				
			String outputLine = "";
			
			for(StationaryDevice n: stationary){
				outputLine += n.type + ","+ n.make + ","+ n.model + "," + n.yearManufactered + ","+ n.MACNumber + ","+ n.color + ","+ n.room + "\n";
				
			}
							
				FileWriter output = new FileWriter(stationaryPath); 
				
				    output.write(outputLine);
				    output.close();				
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();			
			}
			
		}else{			
			// This message is printed in case of absence an appropriate file
			System.out.println("Sorry such file doesn't exists in our records");		
		}		
	}	
	
	
	
	/* --------------------------------
	 * Method no.6  deleteAllMobileDevices
	 * -------------------------------- */	
	/*
	 * This method will delete all mobile devices from text file 
	 * 
	 */
	
	 public void deleteAllMobileDevices(){		 
		// File object is created
			File mobileFile = new File(mobilePath);
			// this condition checks if file is created
			if(mobileFile.exists()){
			
				// Reader is created
				FileWriter mobilewriter = null;				
				try {					
						mobilewriter = new FileWriter(mobileFile);
						System.out.println("All records has been deleted");
				
				} catch (FileNotFoundException e1) {} catch (IOException e) {
						e.printStackTrace();
				}					
			}else{			
				// This message is printed in case of absence an appropriate file
				System.out.println("Sorry such file doesn't exists in our records");
			}	
	 }	 
	 
	
	/* --------------------------------
	 * Method no.7  deleteAllStationaryDevices
	 * -------------------------------- */	
	/*
	 * This method will delete all stationary devices from text file 
	 */
	
	 public void deleteAllStationaryDevices(){
		 
		    // File object is created
			File mobileFile = new File(stationaryPath);
			// this condition checks if file is created
				if(mobileFile.exists()){
					
					// Reader is created
					FileWriter mobilewriter = null;
						
					try {					
							mobilewriter = new FileWriter(mobileFile);	
							System.out.println("All records has been deleted");
						
					} catch (FileNotFoundException e1) {} catch (IOException e) {
							e.printStackTrace();
					}	
						
				}else{			
					// This message is printed in case of absence an appropriate file
					System.out.println("Sorry such file doesn't exists in our records");
				}		 
	 }
	 
	/* --------------------------------
	 * Method no. 8 addMobileDevice
	 * -------------------------------- */	
	/*
	 * This method will add new device to text file 
	 * 
	 */
	
	@Override
	public void addMobileDevice() {		
		
		/* First it is necessary to create new object device  and to List of devices 
		 * For this I going to need Scanner object for user input*/
		
		Scanner key2 = new Scanner(System.in);
		
			/* This is variables that will hold detail of device to be created below */
			String type             = "";
			String make             = "";
			String model            = "";
			String yearManufactered = "";
			String MACNumber        = "";
			String color            = "";
			String owner            = "";
			
			String newDevice        = "";
			
			/* This block will present line interface for user to provide details of new device*/
			System.out.print("\nPlease provide details of device you want to add:\n");
			System.out.print("-------------------------------------------------\n");
			System.out.print("Type: ");                             type = key2.nextLine();
			System.out.print("Producent: ");					    make = key2.nextLine();
			System.out.print("Model: ");	            		   model = key2.nextLine();
			System.out.print("Year of production: ");	yearManufactered = key2.nextLine();
			System.out.print("MAC no.: ");			 	       MACNumber = key2.nextLine();
			System.out.print("Color: ");						   color = key2.nextLine();
			System.out.print("Name of owner: ");				   owner = key2.nextLine();
		
			
			/* I will use this string to write device into file*/
			newDevice = type             + ","
					  + make             + ","
					  + model            + ","
					  + yearManufactered + ","
					  + MACNumber        + ","
					  + color            + ","
					  + owner            ;
			
			// Using above values object of mobile class is created ...
			MobileDevice m1 = new MobileDevice(type, make,  model, yearManufactered,  MACNumber, color, owner);
			// ... and stored in List of devices
			mobile.add(m1);			
			
			File fw = new File(mobilePath);			
			// Condition checks if file exists and if now it will be created
			if(!fw.exists()){
				try {
					fw.createNewFile();
				} catch (IOException e) {				
					e.printStackTrace();
				}
			}else{			
				
				try(PrintWriter output = new PrintWriter(new FileWriter(mobilePath,true))) 
				{	
				    output.printf("%s\r\n", newDevice);
				} 
				catch (Exception e) {}			
			}
			mobile.clear();
	}
	
	/* --------------------------------
	 * Method no. 9 addStationaryDevice
	 * -------------------------------- */	
	/*
	 * This method will add new device to text file 
	 * 
	 */
	
	@Override
	public void addStationaryDevice(){			
		/* First it is necessary to create new object device and add to List of devices 
		 * For this I going to need Scanner object for user input*/
		
		Scanner key2 = new Scanner(System.in);
		
			/* This is variables that will hold detail of device to be created below */
			String type             = "";
			String make             = "";
			String model            = "";
			String yearManufactered = "";
			String MACNumber        = "";
			String color            = "";
			String room             = "";
			
			String newDevice        = "";
			
			/* This block will present line interface for user to provide details of new device*/
			System.out.print("\nPlease provide details of device you want to add:\n");
			System.out.print("-------------------------------------------------\n");
			System.out.print("Type: ");                             type = key2.nextLine();
			System.out.print("Producent: ");					    make = key2.nextLine();
			System.out.print("Model: ");	            		   model = key2.nextLine();
			System.out.print("Year of production: ");	yearManufactered = key2.nextLine();
			System.out.print("MAC no.: ");			 	       MACNumber = key2.nextLine();
			System.out.print("Color: ");						   color = key2.nextLine();
			System.out.print("Position of device in the office: "); room = key2.nextLine();
			
			/* I will use this string to write device into file*/
			newDevice = type             + ","
					  + make             + ","
					  + model            + ","
					  + yearManufactered + ","
					  + MACNumber        + ","
					  + color            + ","
					  + room             ;
			
			// Using above values object of mobile class is created ...
			StationaryDevice s1 = new StationaryDevice(type, make,  model, yearManufactered,  MACNumber, color, room);
			// ... and stored in List of devices
			stationary.add(s1);
			
			System.out.println(stationary.size());
			
			File fw = new File(stationaryPath);
			
			// Condition checks if file exists and if now it will be created
			if(!fw.exists()){
				try {
					fw.createNewFile();
				} catch (IOException e) {				
					e.printStackTrace();
				}
			}else{	
				try(PrintWriter output = new PrintWriter(new FileWriter(stationaryPath,true))) 
				{	
				    output.printf("%s\r\n", newDevice);
				} 
				catch (Exception e) {}
			}
			stationary.clear();
	}
}



