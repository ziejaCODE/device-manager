package deviceManager;

public interface UserInterface3 {
	
	// This method should provide interface that connects all together
	public void userInterfacePanel();
	
	// Method no.1 this method should display all mobile devices that are currently used in the office 
	public void showAllDevices();
	
	// Method no.2 this method should display all mobile devices that are currently used in the office 
	public void showMobileDevices();
	
	// Method no.3 this method should display all mobile devices that are currently used in the office
	public void showStationaryDevices();
	
	// Method no.4 this method should search and display a particular mobile devices that are currently 
	// used in the office and delete it if necessary
	public void searchMobileDevice();
	
	// Method no.5 this method should search and display a particular stationary devices that are currently 
	// used in the office and delete it if necessary
	public void searchStationaryDevice();
	
	// Method no.6 this method will delete all mobile devices
	public void deleteAllMobileDevices();
	
	// Method no.6 this method will delete all stationary devices
	public void deleteAllStationaryDevices();		
	
	// Method no.8 this method should add mobile devices that are currently in the office
	public void addMobileDevice();
	
	// Method no.9 this method should display all stationary devices that are currently in the office 
	public void addStationaryDevice(); 
	
	
	
	
}
