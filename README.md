

OOD Assignment



Main goal of assignment

Create a program that manages information about devices within an office. The
information about the devices should include but not limited to make, model, year
manufactured and MAC address. If the device is a mobile device, like a Smart watch, then
the owner�s name should also be included. Similarly, if the device is a stationary device, like
a printer, then the name of the room the device is located should be included


Below points present requirements for this application and
provide short description of how solved them.


1. The program should organise the information into two categories; mobile devices and 
stationary devices
2. The program should enable a user to create several instances of the devices from each 
category, add their details and store the details in a text file
3. The program should enable a user to view the details of all the devices
4. The program should enable a user to search for a particular device and display its details
5. The program should enable a user to delete all the devices
6. The program should enable a user to delete a  particular device